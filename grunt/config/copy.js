// https://github.com/gruntjs/grunt-contrib-copy
module.exports = {
	css: {
		files: [
			{
				cwd: '<%= paths.tmp %>',
				expand: true,
				flatten: false,
				src: ['**/*','!style*.css', '!style*.map'],
				dest: '<%= paths.theme %>/css',
				filter: 'isFile'
			}
		]
	},
	theme: {
		files: [
			{
				cwd: '<%= paths.tmp %>',
				expand: true,
				flatten: false,
				src: ['style*.css', 'style*.map'],
				dest: '<%= paths.theme %>',
				filter: 'isFile'
			}
		]
	},
	customizer: {
		files: [
			{
				cwd: '<%= paths.authorAssets %>js/',
				expand: true,
				flatten: false,
				src: ['customizer.js'],
				dest: '<%= paths.theme %>js/',
				filter: 'isFile'
			}
		]
	},
	font: {
		files: [
			{
				expand: true,
				flatten: true,
				src: [
                    '<%= paths.bower %>font-awesome/fonts/FontAwesome.eot',
					'<%= paths.bower %>font-awesome/fonts/fontawesome-webfont.eot',
					'<%= paths.bower %>font-awesome/fonts/fontawesome-webfont.svg',
					'<%= paths.bower %>font-awesome/fonts/fontawesome-webfont.ttf',
					'<%= paths.bower %>font-awesome/fonts/fontawesome-webfont.woff',
					'<%= paths.authorAssets %>font/petals.eot',
					'<%= paths.authorAssets %>font/petals.svg',
					'<%= paths.authorAssets %>font/petals.ttf',
					'<%= paths.authorAssets %>font/petals.woff'
				],
				dest: '<%= paths.theme %>font/'
			}
		]
	},
	hybridcore: {
		files: [
			{
				cwd: '<%= paths.composer %>justintadlock/hybrid-core',
				expand: true,
				src: ['**/*'],
				dest: '<%= paths.hybridCore %>'
			}
		]
	},
	flagshiplibrary: {
		files: [
			{
				cwd: '<%= paths.composer %>flagshipwp/flagship-library',
				expand: true,
				src: ['**/*'],
				dest: '<%= paths.theme %>includes/vendor/flagship-library'
			}
		]
	},
	themehookalliance: {
		files: [
			{
				cwd: '<%= paths.composer %>zamoose/themehookalliance',
				expand: true,
				src: ['tha-theme-hooks.php'],
				dest: '<%= paths.theme %>includes/vendor/'
			}
		]
	},
	images: {
		files: [
			{
				cwd: '<%= paths.tmp %>images',
				expand: true,
				flatten: true,
				src: ['*', '!screenshot.png'],
				dest: '<%= paths.theme %>images',
				filter: 'isFile'
			}
		]
	},
	screenshot: {
		files: [
			{
				cwd: '<%= paths.tmp %>images',
				expand: true,
				flatten: true,
				src: ['screenshot.png'],
				dest: '<%= paths.theme %>',
				filter: 'isFile'
			}
		]
	},
	languages: {
		files: [
			{
				cwd: '<%= paths.assets %><%= paths.languages %>',
				expand: true,
				src: ['*.po'],
				dest: '<%= paths.theme%><%= paths.languages %>',
				filter: 'isFile'
			}
		]
	}
};
