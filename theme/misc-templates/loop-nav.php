<?php
/**
 * A template part to display single entry navigation and pagination for archives.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */
?>

<?php if ( is_singular( 'post' ) ) : ?>

	<nav class="nav-single">

		<?php previous_post_link( '<span class="nav-previous">' . __( '%link', 'pink-petals' ) . '</span>', '&larr; Previous Post' ); ?>
		<?php next_post_link(     '<span class="nav-next">' . __( '%link', 'pink-petals' ) . '</span>', 'Next Post &rarr;' ); ?>
	</nav><!-- .nav-single -->

	<?php

endif;

if ( is_home() || is_archive() || is_search() ) :

	if ( current_theme_supports( 'loop-pagination' ) ) :
		loop_pagination(
			array(
				'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Page', 'pink-petals' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next Page', 'pink-petals' ) . '</span>',
			)
		);
	else : 
	?>
	
	<nav class="nav-archive">
		<?php previous_post_link( '<span class="nav-previous">' . __( '%link', 'pink-petals' ) . '</span>', '&larr; Older Posts' ); ?>
		<?php next_post_link(     '<span class="nav-next">' . __( '%link', 'pink-petals' ) . '</span>', 'Newer Posts &rarr;' ); ?>
	</nav><!-- .nav-archive -->
	
	<?php
	endif;

endif;
