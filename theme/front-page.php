<?php
/**
 * The static front page template.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */
?>

<?php get_header(); ?>

<div <?php hybrid_attr( 'site-inner' ); ?>>

	<?php hybrid_get_menu( 'breadcrumbs' ); ?>

	<?php if ( is_active_sidebar( 'home-1' ) ) : ?>

		<div <?php hybrid_attr( 'home-1' ); ?>>

			<?php dynamic_sidebar( 'home-1' ); ?>

		</div><!-- .header-right -->

	<?php endif; ?>

	<?php if ( is_active_sidebar( 'home-2' ) ) : ?>

		<div <?php hybrid_attr( 'home-2' ); ?>>

			<?php dynamic_sidebar( 'home-2' ); ?>

		</div><!-- .header-right -->

	<?php endif; ?>

	<?php if ( is_active_sidebar( 'home-3' ) ) : ?>

		<div <?php hybrid_attr( 'home-3' ); ?>>

			<?php dynamic_sidebar( 'home-3' ); ?>

		</div><!-- .header-right -->

	<?php endif; ?>

	<?php tha_content_before(); ?>

	<main <?php hybrid_attr( 'content' ); ?>>

		<?php tha_content_top(); ?>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php hybrid_get_content_template(); ?>

			<?php endwhile; ?>

			<?php get_template_part( 'misc-templates/loop-nav' ); ?>

		<?php else : ?>

			<?php get_template_part( 'content/error' ); ?>

		<?php endif; ?>

		<?php tha_content_bottom(); ?>

	</main><!-- #content -->

	<?php tha_content_after(); ?>

	<?php hybrid_get_sidebar( 'primary' ); ?>

</div><!-- #site-inner -->

<?php
get_footer();
