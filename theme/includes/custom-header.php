<?php
/**
 * Handles the setup and usage of the WordPress custom headers feature.
 *
 * @package    PinkPetals
 * @author     Jenny Ragan <jenny@jennyyragan.com>
 * @copyright  Copyright (c) 2014, Jenny Ragan
 * @link       http://djrthemes.com/themes/petals
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

 * Based on custom-header.php from Stargazer theme by Justin Tadlock <justin@justintadlock.com>,
 * Copyright (c) 2013 - 2014, Justin Tadlock, http://themehybrid.com/themes/stargazer
 */

/* Call late so child themes can override. */
add_action( 'after_setup_theme', 'pink_petals_custom_header_setup', 20 );

/**
 * Adds support for the WordPress 'custom-header' theme feature and registers custom headers.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function pink_petals_custom_header_setup() {

	/* Adds support for WordPress' "custom-header" feature. */
	add_theme_support(
		'custom-header',
		array(
			'random-default'         => false,
			'flex-width'             => true,
			'flex-height'            => true,
			'header-text'            => true,
			'uploads'                => true,
			'width'                  => '1000px'
		)
	);
}

add_action( 'customize_register', 'pink_petals_customize_register_header' );
/**
 * Add postMessage support for header_text for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function pink_petals_customize_register_header( $wp_customize ) {
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}