<?php
/**
 * Script and Style Loaders and Related Functions.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2015 DJRthemes, Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://djrthemes.com/
 * @since       1.0.0
 */

add_action( 'wp_enqueue_scripts', 'pink_petals_rtl_add_data' );
/**
 * Replace the default theme stylesheet with a RTL version when a RTL
 * language is being used.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function pink_petals_rtl_add_data() {
	wp_style_add_data( 'style', 'rtl', 'replace' );
	wp_style_add_data( 'style', 'suffix', hybrid_get_min_suffix() );
}

add_action( 'wp_enqueue_scripts', 'pink_petals_enqueue_styles' );
/**
 * Enqueue theme styles.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function pink_petals_enqueue_styles() {
	$css_dir = trailingslashit( get_template_directory_uri() ) . 'css/';
	$suffix  = hybrid_get_min_suffix();

	wp_enqueue_style( 'petals-google-fonts', '//fonts.googleapis.com/css?family=Lora:400,700|Gudea:400,700,400italic|Indie+Flower' );
	wp_enqueue_style( 'font-awesome', $css_dir . "font-awesome{$suffix}.css", array(), '4.2.0' );
}

add_action( 'wp_enqueue_scripts', 'pink_petals_enqueue_scripts' );
/**
 * Enqueue theme scripts.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function pink_petals_enqueue_scripts() {
	$js_dir = trailingslashit( get_template_directory_uri() ) . 'js/';
	$suffix = hybrid_get_min_suffix();

	wp_enqueue_script( 'pink_petals_theme', $js_dir . "theme{$suffix}.js", array( 'jquery' ), '1.0.0', true );
}

add_editor_style( pink_petals_get_editor_styles() );
/**
 * Callback function for adding editor styles.  Use along with the add_editor_style() function.
 *
 * @since  1.0.0
 * @access public
 * @return array
 */
function pink_petals_get_editor_styles() {
	/* Set up an array for the styles. */
	$editor_styles = array();
	/* Add the theme's editor styles. */
	$editor_styles[] = trailingslashit( get_template_directory_uri() ) . 'css/editor-style.css';
	/* If a child theme, add its editor styles. Note: WP checks whether the file exists before using it. */
	if ( is_child_theme() && file_exists( trailingslashit( get_stylesheet_directory() ) . 'css/editor-style.css' ) )
		$editor_styles[] = trailingslashit( get_stylesheet_directory_uri() ) . 'css/editor-style.css';
	/* Add the locale stylesheet. */
	$editor_styles[] = get_locale_stylesheet_uri();
	$font_url = '//fonts.googleapis.com/css?family=Lora:400,700|Gudea:400,700,400italic|Indie+Flower';
	$font_url = str_replace( ',', '%2C', $font_url );
	$editor_styles[] = $font_url;
	/* Return the styles. */
	return $editor_styles;
}

add_filter( 'tiny_mce_before_init', 'pink_petals_tiny_mce_before_init' );
/**
 * Adds the <body> class to the visual editor.
 *
 * @since  1.0.0
 * @access public
 * @param  array  $settings
 * @return array
 */
function pink_petals_tiny_mce_before_init( $settings ) {
	$settings['body_class'] = join( ' ', array_merge( get_body_class(), get_post_class() ) );
	return $settings;
}

add_action( 'customize_preview_init', 'pink_petals_customize_preview_js' );
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * @since  1.0.0
 * @access public
 * @param  array  $settings
 * @return array
 */
function pink_petals_customize_preview_js() {
	$js_dir = trailingslashit( get_template_directory_uri() ) . 'js/';
	$suffix = hybrid_get_min_suffix();

	wp_enqueue_script( 'pink_petals_customizer', $js_dir . "customizer{$suffix}.js", array( 'customize-preview' ), '1.0.0', true );
}