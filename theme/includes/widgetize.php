<?php
/**
 * Register and Display Widget Areas.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */

add_action( 'widgets_init', 'pink_petals_register_sidebars', 5 );
/**
 * Registers sidebars.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function pink_petals_register_sidebars() {
	hybrid_register_sidebar(
		array(
			'id'          => 'primary',
			'name'        => _x( 'Primary Sidebar', 'sidebar', 'pink-petals' ),
			'description' => __( 'The main sidebar. It is displayed on either the left or right side of the page based on the chosen layout.', 'pink-petals' ),
		)
	);
	hybrid_register_sidebar(
		array(
			'id'            => 'footer-left',
			'name'          => _x( 'Footer Left', 'sidebar', 'pink-petals' ),
			'description'   => __( 'The footer left sidebar area.', 'pink-petals' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widget-title">',
			'after_title'   => '</span>',
		)
	);
	hybrid_register_sidebar(
		array(
			'id'            => 'home-1',
			'name'          => _x( 'Home Page 1', 'sidebar', 'pink-petals' ),
			'description'   => __( 'First Home Page Widget Area.', 'pink-petals' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widget-title">',
			'after_title'   => '</span>',
		)
	);
	hybrid_register_sidebar(
		array(
			'id'            => 'home-2',
			'name'          => _x( 'Home Page 2', 'sidebar', 'pink-petals' ),
			'description'   => __( 'Second Home Page Widget Area.', 'pink-petals' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widget-title">',
			'after_title'   => '</span>',
		)
	);
	hybrid_register_sidebar(
		array(
			'id'            => 'home-3',
			'name'          => _x( 'Home Page 3', 'sidebar', 'pink-petals' ),
			'description'   => __( 'Third Home Page Widget Area.', 'pink-petals' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<span class="widget-title">',
			'after_title'   => '</span>',
		)
	);
}
