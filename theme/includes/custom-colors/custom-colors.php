<?php
/**
 * Handles the custom colors feature for the theme.  This feature allows the theme or child theme author to
 * set a custom color by default.  However the user can overwrite this default color via the theme customizer
 * to a color of their choosing.

 * @package    PinkPetals
 * @author     Jenny Ragan <jenny@jennyyragan.com>
 * @copyright  Copyright (c) 2014, Jenny Ragan
 * @link       http://djrthemes.com/themes/petals
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

 * Based on custom colors from Stargazer theme by Justin Tadlock <justin@justintadlock.com>,
 * Copyright (c) 2013 - 2014, Justin Tadlock, http://themehybrid.com/themes/stargazer
 */
/**
 * Handles custom theme color options via the WordPress theme customizer.
 *
 * @since  1.0.0
 * @access public
 */
final class PinkPetals_Custom_Colors {
	/**
	 * Holds the instance of this class.
	 *
	 * @since  1.0.0
	 * @access private
	 * @var    object
	 */
	private static $instance;
	/**
	 * Sets up the Custom Colors Palette feature.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		/* Output CSS into <head>. */
		add_action( 'wp_head', array( $this, 'wp_head_callback' ) );
		/* Add a '.custom-colors' <body> class. */
		add_filter( 'body_class', array( $this, 'body_class' ) );
		/* Add options to the theme customizer. */
		add_action( 'customize_register', array( $this, 'customize_register' ) );
		/* Filter the default primary color late. */
		add_filter( 'theme_mod_color_primary', array( $this, 'color_primary_default' ), 95 );
		/* Filter the default secondary color late. */
		add_filter( 'theme_mod_color_secondary', array( $this, 'color_secondary_default' ), 95 );
		/* Filter the default color scheme late. */
		add_filter( 'theme_mod_color_scheme', array( $this, 'color_scheme_default' ), 95 );
		/* Delete the cached data for this feature. */
		add_action( 'update_option_theme_mods_' . get_stylesheet(), array( $this, 'cache_delete' ) );
		/* Visual editor colors */
		add_action( 'wp_ajax_petals_editor_styles',         array( $this, 'editor_styles_callback' ) );
		add_action( 'wp_ajax_no_priv_petals_editor_styles', array( $this, 'editor_styles_callback' ) );
	}
	/**
	 * Returns a default primary color if there is none set.  We use this instead of setting a default
	 * so that child themes can overwrite the default early.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  string  $hex
	 * @return string
	 */
	public function color_primary_default( $hex ) {
		return $hex ? $hex : 'df8ba8';
	}
	/**
	 * Returns a default secondary color if there is none set.  We use this instead of setting a default
	 * so that child themes can overwrite the default early.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  string  $hex
	 * @return string
	 */
	public function color_secondary_default( $hex ) {
		return $hex ? $hex : 'dcb762';
	}
	/**
	 * Returns a default color scheme color if there is none set.  We use this instead of setting a default
	 * so that child themes can overwrite the default early.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  string  $scheme
	 * @return string
	 */
	public function color_scheme_default( $scheme ) {
		return $scheme ? $scheme : 'pink';
	}
	/**
	 * Adds the 'custom-colors' class to the <body> element.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  array  $classes
	 * @return array
	 */
	public function body_class( $classes ) {
		$classes[] = 'custom-colors';
		if ( get_theme_mod( 'color_skin_bg', 1 ) ) {
			$classes[] = 'color-skin';
			$skin = get_theme_mod( 'color_scheme', apply_filters( 'theme_mod_color_scheme', '' ) );
			$classes[] = esc_attr("skin-$skin");
		}
		return $classes;
	}
	/**
	 * Callback for 'wp_head' that outputs the CSS for this feature.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function wp_head_callback() {
		$stylesheet = get_stylesheet();
		/* Get the cached style. */
		$style = wp_cache_get( "{$stylesheet}_custom_colors" );
		/* If the style is available, output it and return. */
		if ( !empty( $style ) ) {
			echo $style;
			return;
		}
		$style = $this->get_styles();
		/* Put the final style output together. */
		$style = "\n" . '<style type="text/css" id="custom-colors-css">' . trim( $style ) . '</style>' . "\n";
		/* Cache the style, so we don't have to process this on each page load. */
		wp_cache_set( "{$stylesheet}_custom_colors", $style );
		/* Output the custom style. */
		echo $style;
	}
	/**
	 * Ajax callback for outputting the primary styles for the WordPress visual editor.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function editor_styles_callback() {
		header( 'Content-type: text/css' );
		echo $this->get_styles();
		die();
	}
	/**
	 * Compiles styles with scss
	 * Uses scss.inc.php from Jetpack
	 * https://github.com/Automattic/jetpack
	 * http://leafo.net/scssphp
	 *
	 * @since  1.0.0
	 * @access public
	 * @return string
	 */
	public function get_styles() {

		require_once( trailingslashit( dirname( __FILE__ ) ) . 'scss.inc.php' );

		$compiler = new scssc();

		$sass = $this->get_scss();
		try {
			$compiler->setFormatter( 'scss_formatter_compressed' );
			return $compiler->compile( $sass );
		} catch ( Exception $e ) {
			return false;
		}
	}

	public function get_scss() {

		$primary = get_theme_mod( 'color_primary', apply_filters( 'theme_mod_color_primary', '' ) );
		$secondary = get_theme_mod( 'color_secondary', apply_filters( 'theme_mod_color_secondary', '' ) );
		$skin = get_theme_mod( 'color_scheme', apply_filters( 'theme_mod_color_scheme', '' ) );

		$sass_template = '';
		$sass_template .= '$primary: #' . esc_attr( $primary ) . ';';
		$sass_template .= '$primary-medium: hsl(hue($primary),80,88);';
		$sass_template .= '$primary-light: hsl(hue($primary),61,89);';
		$sass_template .= '$primary-lighter: hsl(hue($primary),86,97);';
		$sass_template .= '$secondary: #' . esc_attr( $secondary ) . ';';
		$sass_template .= '$secondary-light: hsl(hue($secondary),45,94);';

		ob_start();
		include( trailingslashit( dirname( __FILE__ ) ) . 'colors.scss' );
		$sass_template .= ob_get_clean();
		return $sass_template;
	}

	/**
	 * Registers the customize settings and controls.  We're tagging along on WordPress' built-in
	 * 'Colors' section.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object $wp_customize
	 * @return void
	 */
	public function customize_register( $wp_customize ) {
		/* add graphics option */
		$wp_customize->add_setting(
			'color_skin_bg',
			array(
				'default'              => 1,
				'type'                 => 'theme_mod',
				'capability'           => 'edit_theme_options',
				'sanitize_callback'    => 'absint',
				'sanitize_js_callback' => 'absint',
				//'transport'            => 'postMessage',
			)
		);
		/* Add a control for skins. */
		$wp_customize->add_control( 'custom-colors-skin-bg', array(
			'settings' => 'color_skin_bg',
			'label'   => 'Enable skin background graphics',
			'section' => 'colors',
			'type'    => 'checkbox'
		));
		/* Add a new setting for this color. */
		$wp_customize->add_setting(
			'color_scheme',
			array(
				'default'              => apply_filters( 'theme_mod_color_scheme', '' ),
				'type'                 => 'theme_mod',
				'capability'           => 'edit_theme_options',
				'sanitize_callback'    => 'esc_attr',
				'sanitize_js_callback' => 'esc_attr',
				//'transport'            => 'postMessage',
			)
		);
		/* Add a control for skins. */
		$wp_customize->add_control( 'custom-colors-scheme', array(
			'settings' => 'color_scheme',
			'label'   => 'Select a skin:',
			'section' => 'colors',
			'type'    => 'select',
			'choices'    => array(
				'pink' => 'Pink',
				'blue' => 'Blue',
				'green' => 'Green',
				'purple' => 'Purple',
				'gold' => 'Gold',
				'silver' => 'Silver'
			),
		));
		/* Add a new setting for this color. */
		$wp_customize->add_setting(
			'color_primary',
			array(
				'default'              => apply_filters( 'theme_mod_color_primary', '' ),
				'type'                 => 'theme_mod',
				'capability'           => 'edit_theme_options',
				'sanitize_callback'    => 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' => 'maybe_hash_hex_color',
				//'transport'            => 'postMessage',
			)
		);
		/* Add a control for this color. */
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'custom-colors-primary',
				array(
					'label'    => esc_html__( 'Primary Color', 'pink-petals' ),
					'section'  => 'colors',
					'settings' => 'color_primary',
					'priority' => 20,
				)
			)
		);
		/* Add a new setting for this color. */
		$wp_customize->add_setting(
			'color_secondary',
			array(
				'default'              => apply_filters( 'theme_mod_color_secondary', '' ),
				'type'                 => 'theme_mod',
				'capability'           => 'edit_theme_options',
				'sanitize_callback'    => 'sanitize_hex_color_no_hash',
				'sanitize_js_callback' => 'maybe_hash_hex_color',
				//'transport'            => 'postMessage',
			)
		);
		/* Add a control for this color. */
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'custom-colors-secondary',
				array(
					'label'    => esc_html__( 'Secondary Color', 'pink-petals' ),
					'section'  => 'colors',
					'settings' => 'color_secondary',
					'priority' => 20,
				)
			)
		);
	}
	/**
	 * Deletes the cached style CSS that's output into the header.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function cache_delete() {
		wp_cache_delete( get_stylesheet() . '_custom_colors' );
	}

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {
		if ( !self::$instance )
			self::$instance = new self;
		return self::$instance;
	}
}
PinkPetals_Custom_Colors::get_instance();