<?php
/**
 * Customizer Options
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, DJRthemes, LLC
 * @license     GPL-2.0+
 * @link        http://djrthemes.com/
 * @since       1.0.0
 */