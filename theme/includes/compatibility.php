<?php
/**
 * Plugin Compatibility File
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */

add_action( 'after_setup_theme', 'pink_petals_jetpack_setup', 12 );
/**
 * Make adjustments to the theme when Jetpack is installed and activated.
 *
 * @since  1.0.0
 * @return void
 */
function pink_petals_jetpack_setup() {
	// Return early if Jetpack isn't activated.
	if ( ! class_exists( 'Jetpack' ) ) {
		return;
	}

	// Add support for Infinite Scroll.
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );

	// Get the active jetpack modules.
	$modules   = get_option( 'jetpack_active_modules' );

	// List all modules which conflict with the Cleaner Gallery feature.
	$conflicts = array(
		'tiled-gallery',
		'carousel',
	);

	// Remove Cleaner Gallery support if any conflicting modules are active.
	if ( count( array_intersect( $conflicts, (array) $modules ) ) !== 0 ) {
		remove_theme_support( 'cleaner-gallery' );
	}
}

//add_filter( 'soliloquy_pre_data', 'pink_petals_pre_data', 10, 2 );
/**
 * Returns slider width and height for cropping to set home page slider design
 * @since 1.0.0
 * @param $data
 * @param $slider_id
 *
 * @return array
 */
function pink_petals_pre_data( $data, $slider_id ) {

	if ( $slider_id == intval( get_theme_mod( 'pink_petals_slider' ) ) ) { // only apply to the choosen home page slider
		$data['config']['slider_width'] = 733;
		$data['config']['slider_height'] = 405;
	}

	return $data;
}

add_filter( 'soliloquy_slider_themes', 'pink_petals_soliloquy_themes' );
/**
 * @since  1.0.0
 * Adds custom theme to soliloquy
 * @param $themes
 * @return array with theme added
 */
function pink_petals_soliloquy_themes( $themes ) {
	$themes[] =
		array(
			'value' => 'petals',
			'name'  => __( 'Petals', 'pink-petals' ),
			'file'  => '' // this is useless since we aren't putting our theme in the plugins theme folder
		);

	return $themes;
}

add_action( 'wp_footer', 'pink_petals_remove_theme_css', 1 );
/**
 * @since  1.0.0
 * hacky solution to keep soliloquy from enqueuing theme style since it is in our theme css already,
 * using soliloquy_before_output hook simple because it is the first hook after soliloquy
 * does the enqueue
 *
 */
function pink_petals_remove_theme_css() {
	// this is pretty hacky since we can't know for sure the plugin name
	wp_dequeue_style( 'soliloquy-litepetals-theme' );
	wp_dequeue_style( 'soliloquypetals-theme' );
}

