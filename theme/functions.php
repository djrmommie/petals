<?php
/**
 * Theme Setup Functions and Definitions.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */

// Include Hybrid Core.
require_once( trailingslashit( get_template_directory() ) . 'hybrid-core/hybrid.php' );
new Hybrid();

add_action( 'after_setup_theme', 'pink_petals_setup', 5 );
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since   1.0.0
 * @return  void
 */
function pink_petals_setup() {

	// Set the includes directories.
	$includes_dir = get_template_directory() . '/includes';

	// Add Support for Theme layouts.
	add_theme_support(
		'theme-layouts',
		array(
			'1c'        => __( '1 Column Wide',                'pink-petals' ),
			'2c-l'      => __( '2 Columns: Content / Sidebar', 'pink-petals' ),
			'2c-r'      => __( '2 Columns: Sidebar / Content', 'pink-petals' )
		),
		array( 'default' => is_rtl() ? '2c-r' :'2c-l' )
	);

	// Handle content width for embeds and images.
	hybrid_set_content_width( 1000 );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Load theme styles.
	add_theme_support( 'hybrid-core-styles', array( 'google-fonts', 'parent', 'style' )	);

	// Add navigation menus.
	register_nav_menu( 'after-header', _x( 'Header Menu', 'nav menu location', 'pink-petals' ) );
	register_nav_menu( 'social',    _x( 'Social',    'nav menu location', 'pink-petals' ) );

	$formats = array(
		'aside',
		'gallery',
		'link',
		'image',
		'quote',
		'status',
		'video',
		'audio',
		'chat',
	);

	// Add support for Post Formats.
	add_theme_support( 'post-formats', $formats );

	// Add support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// Add support for easer image usage.
	add_theme_support( 'get-the-image' );

	// Add a nicer [gallery] shortcode implementation.
	add_theme_support( 'cleaner-gallery' );

	// Add better captions for themes to style.
	add_theme_support( 'cleaner-caption' );

	// Add support for loop pagination.
	add_theme_support( 'loop-pagination' );

	// Add support for hybrid core template hierarchy.
	add_theme_support( 'hybrid-core-template-hierarchy' );

	// Add support for flagship footer widgets.
	add_theme_support( 'flagship-footer-widgets', 4 );

	/* Custom background. */
	add_theme_support(
		'custom-background',
		array( 'default-color' => 'ffffff' )
	);

	// site logo
	add_theme_support( 'site-logo', array( 'size' => 'full', ) );

	add_theme_support( 'typecase', array(
		// array of simple options
		// these options are displayed in the main theme fonts panel section
		// array of simple options
		'simple' => array(
			// the label displayed in customizer
			array(
				'label' => 'Body',
				// the CSS selector to apply the font to
				'selector' => 'body, .body-font, .whistles',
				// the default font for the selector
				'default' => 'Gudea',
			),
			array(
				'label' => 'Headings',
				'selector' => 'h1, h2, h3, h4, h5, h6, button, input[type="button"], .nav-next, .nav-previous, input[type="reset"], input[type="submit"], .button, .nav-menu.after-header, .heading-font, .whistles-tabs .whistles-tabs-nav li a, .whistles-toggle .whistle-title',
				'default' => '"Indie Flower"',
			),
			array(
				'label' => 'Site Title',
				'selector' => '.site-title, .site-description',
				'default' => 'Lora',
			)
		),
		// array of advanced options
		// hidden by default, can be enabled by the user
		'advanced' => array(
			// each array is a customizer section in the theme fonts panel
			'general' => array(
				// each array is an option within the customizer section
				array(
					// the label displayed in customizer
					'label' => 'Site Title',
					// the CSS selector to apply the font to
					'selector' => '.site-title',
					// the default font for the selector
					'default' => 'Lora',
				),
				array(
					'label' => 'Site Description',
					'selector' => '.site-description',
					'default' => 'Lora',
				),
			),
			'content' => array(
				array(
					'label' => 'Article Title',
					'selector' => 'h1.entry-title',
					'default' => '"Indie Flower"',
					'description' => 'Large heading at the top of pages and posts',
				),
				array(
					'label' => 'Block Quote',
					'selector' => 'blockquote',
					'default' => 'Gudea',
				)
			),
			'sidebar' => array(
				array(
					'label' => 'Widget Content',
					'selector' => '.widget',
					'default' => 'Gudea',
				),
				array(
					'label' => 'Widget Titles',
					'selector' => 'h2.widget-title',
					'default' => '"Indie Flower"',
				)
			)
		)
	));
}

add_action( 'after_setup_theme', 'pink_petals_includes', 10 );
/**
 * Load all required theme files.
 *
 * @since   1.0.0
 * @return  void
 */
function pink_petals_includes() {
	// Set the includes directories.
	$includes_dir = trailingslashit( get_template_directory() ) . 'includes/';

	// Load the main file in the Flagship library directory.
	require_once $includes_dir . 'vendor/flagship-library/flagship-library.php';
	new Flagship_Library;

	// Load all PHP files in the vendor directory.
	require_once $includes_dir . 'vendor/tha-theme-hooks.php';

	// Load all PHP files in the includes directory.
	require_once $includes_dir . 'compatibility.php';
	require_once $includes_dir . 'general.php';
	require_once $includes_dir . 'scripts.php';
	require_once $includes_dir . 'widgetize.php';
	require_once $includes_dir . 'custom-colors/custom-colors.php';
	require_once $includes_dir . 'custom-header.php';
}

// Add a hook for child themes to execute code.
do_action( 'flagship_after_setup_parent' );