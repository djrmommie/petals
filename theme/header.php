<?php
/**
 * The Header for our theme.
 *
 * @package     PinkPetals
 * @subpackage  HybridCore
 * @copyright   Copyright (c) 2014, Flagship, LLC
 * @license     GPL-2.0+
 * @link        http://flagshipwp.com/
 * @since       1.0.0
 */
?>
<!DOCTYPE html>
<?php tha_html_before(); ?>
<html <?php language_attributes( 'html' ); ?>>

<head>
<?php tha_head_top(); ?>
<?php wp_head(); ?>
<?php tha_head_bottom(); ?>
</head>

<body <?php hybrid_attr( 'body' ); ?>>

	<?php tha_body_top(); ?>

	<div <?php hybrid_attr( 'site-container' ); ?>>

		<div class="skip-link">
			<a href="#content" class="button screen-reader-text">
				<?php _e( 'Skip to content (Press enter)', 'pink-petals' ); ?>
			</a>
		</div><!-- .skip-link -->

		<?php tha_header_before(); ?>

		<header <?php hybrid_attr( 'header' ); ?>>

			<div <?php hybrid_attr( 'wrap', 'header' ); ?>>

				<?php tha_header_top(); ?>

				<div <?php hybrid_attr( 'branding' ); ?>>

					<?php if ( get_header_image() ) : ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">
						</a>
					<?php endif; // End header image check. ?>

					<?php if ( current_theme_supports( 'site-logo' ) && ( flagship_has_logo() || is_customize_preview() ) ) : ?>

						<?php flagship_the_logo(); ?>

					<?php endif; ?>

					<?php if ( display_header_text() || is_customize_preview() ) : // If user chooses to display header text. ?>

						<?php hybrid_site_title(); ?>
						<?php hybrid_site_description(); ?>

					<?php endif; // End check for header text. ?>

				</div><!-- #branding -->

				<?php tha_header_bottom(); ?>

			</div>

		</header><!-- #header -->

		<?php tha_header_after(); ?>

		<?php hybrid_get_menu( 'after-header' ); ?>
